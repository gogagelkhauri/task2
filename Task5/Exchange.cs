﻿namespace Task5;

public class Exchange
{
    private readonly Dictionary<string, float> _currencies;

    public Exchange()
    {
        _currencies = new Dictionary<string, float>()
        {
            {"USD", 2.60f },
            {"EUR", 2.90f},
            {"P", 3.20f}
        };
    }

    public Dictionary<string, float> GetCurrencies()
    {
        return _currencies;
    }

    public float SwapFromGEL(float amount, string to)
    {
        var ration = _currencies.FirstOrDefault(x => x.Key == to).Value;
        return amount * ration;
    }
}