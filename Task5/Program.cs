﻿using Task5;

while (true)
{
    var exchange = new Exchange();
    foreach (var currency in exchange.GetCurrencies())
    {
        Console.WriteLine("{0},{1}", currency.Key, currency.Value);
    }
    Console.WriteLine("Please select currency ");
    var curr = Console.ReadLine();
    Console.WriteLine("Please input amount in GEL to convert ");
    var amount = float.Parse(Console.ReadLine());
    Console.WriteLine(exchange.SwapFromGEL(amount, curr));
}