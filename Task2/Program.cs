﻿var data = new Dictionary<string,int[]>()
{
    {"სამუშაო დღე",new int [] {1,2,3,4,5}},
    {"დასვენება",new int [] {6,7}}
};

while (true)
{
    try
    {
        Console.WriteLine("Please enter day from 1 to 7");
        var input = int.Parse(Console.ReadLine());
        if (input < 1 || input > 7)
        {
            throw new Exception("The day is out of range");
        }

        var day = data.FirstOrDefault(x => x.Value.Contains(input)).Key;
        Console.WriteLine(day);

    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}
