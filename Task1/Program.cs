﻿var data = new Dictionary<string, int[]>()
{
    { "Winter", new int[] {12,1,2} },
    { "Spring", new int[] {3,4,5} },
    { "Summer", new int[] {6,7,8} },
    { "Autumn", new int[] {9,10,11} }
};
while (true)
{
    try
    {
    
        Console.WriteLine("Please Input Month from 1 to 12");
        var input = int.Parse(Console.ReadLine());
        if (input < 1 || input > 12)
        {
            throw new Exception("The month is out of range");
        }

        var month = data.FirstOrDefault(x => x.Value.Contains(input)).Key;
        Console.WriteLine(month);

    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}
